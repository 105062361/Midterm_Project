# Software Studio 2018 Spring Midterm Project
## Student ID and Name
* Student ID : 105062361
* Name : 伍瀚翔

## Topic
* Forum
* Key functions
    1. User page
    2. Post page
    3. Post list page
    4. Leave comment under any post
* Other functions
    1. Search a post
    2. Delete post (User can only delete their own post)
    3. Create a post based on category (Post list page with different category)
    4. Vote a post

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Hosting|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
    1.	There is Log in and Sign up button at the top navigation bar.
    2.	One can sign up with email or log in with email, Facebook or Google.
    3.	One can also log out by clicking a dropdown button with user email at the top navigation bar.
    4.	Everyone can view post and comment, but only log in members can create a new post, comment under any post 
        and vote a post. 
    5.	There is a recent post list page when you first see this webpage. You can view post based on category by 
        clicking the corresponding sections at the left sidebar.
    6.	Or you can search a post with keywords with the search bar at the top navigation bar.
    7.	Every user will receive a web push notification when there is someone create a new post.
    8.	There is CSS animation (zoom) when user click log in, sign up, create a new post or write a comment button.
        The corresponding pop out window is come with a zoom animation.
    9.	User page (click “My profile” in a dropdown button with user email) can view a user own post and the 
        commented post.
    10.	In addition, user can delete can only delete their own post, not other people’s post.
    11.	Moreover, I have some adjustment on the CSS layout so that user can have a better view on small device.
        (RWD)
    12.	I do not use template but reuse some lab 6 code for this project and use a lot of bootstrap 4 element for 
        RWD.

## Security Report (Optional)
    1.  Everyone can read the database information.
    2.	However, only the authorised user (membership) can create a new post, comment under any post and vote a post.
    3.	For the webpage design, the “create a new post button” will only show after user signing in their account.
    4.	If the user does not log in, the “comment button” and “vote button” will not be functioned when clicking.
    5.	Last but not least, user can only delete their own posts when hitting “delete button”, not other people’s 
        post.


## Firebase Hosting Website
* https://ss-midterm-project-105062361.firebaseapp.com/
