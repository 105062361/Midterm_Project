var fb = firebase.database().ref("incid");
// monitors changes and updates UI
fb.child('counter').on('value', updateDiv);
// fb.on('value', updatePre);

// creates a new, incremental record
$('#inc').on('click', incId);

// creates a new, incremental record
function incId() {
    // increment the counter
    fb.child('counter').transaction(function(currentValue) {
        return (currentValue||0) + 1
    });
}


// for demo purposes
function updateDiv(ss) {
   $('#likeViewer').text( (ss.val() || 0) + " likes" );
   $('#custom').val('rec'+(parseInt(ss.val(), 10)+1)); 
}

