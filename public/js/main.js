window.onload = function()
{
    init();
    redirectPostWhenClickCategory();
    signUpButton();
    closeButton();
    signInButton();
    closeButtonSI();
    googleSignIn();
    googleSignIn2();
    fbSignIn();
    fbSignIn2();
    post();
    chromeNotification();
    search();
       
}

function signInButton()
{
    var signInBtn = document.querySelector("#signInBtn");  
    signInBtn.addEventListener("click", signInCss);
}

function signUpButton()
{
    var signUpBtn = document.querySelector("#signUpBtn");  
    signUpBtn.addEventListener("click", signUpCss);
    
}

var signInCss = function()
{
    
    document.querySelector(".signIn-wrapper").style.display = "block";
    document.body.style.backgroundColor = "rgba(0, 0, 0, 0.4)";
    document.querySelector(".signUp-wrapper").style.display = 'none';
    // $('body').css({'overflow':'hidden'});
    // $(document).bind('scroll',function () { 
    //      window.scrollTo(0,0); 
    // });
    document.querySelector(".post").style.display = "none";
    $(".comment").hide();
    
}

var signUpCss = function()
{
    //alert("js signUp");
    document.querySelector(".signUp-wrapper").style.display = "block";
    document.body.style.backgroundColor = "rgba(0, 0, 0, 0.4)";
    document.querySelector(".signIn-wrapper").style.display = 'none';
    // $('body').css({'overflow':'hidden'});
    // $(document).bind('scroll',function () { 
    //      window.scrollTo(0,0); 
    // });
    document.querySelector(".post").style.display = "none";
}

function closeButtonpP()
{
    var closeBtnSI = document.querySelector(".closepP");  
    closeBtnSI.addEventListener("click", closeCsspP);
}

function closeButtonSI()
{
    var closeBtnSI = document.querySelector(".closeSI");  
    closeBtnSI.addEventListener("click", closeCssSI);
}

function closeButton()
{
    var closeBtn = document.querySelector(".closeSU");  
    closeBtn.addEventListener("click", closeCss);
}

var closeCsspP = function()
{
    document.querySelector("#postPage").style.display = 'none';
    document.body.style.backgroundColor = "white";
    // //can scroll again
    // $(document).unbind('scroll'); 
    // $('body').css({'overflow':'visible'});
    document.querySelector(".post").style.display = "block";
    post_title = document.querySelector('#postTitle');
    post_txt = document.querySelector('#comment');
    post_txt.value = "";
    post_title.value = "";
}

var closeCssSI = function ()
{
    document.querySelector(".signIn-wrapper").style.display = 'none';
    document.body.style.backgroundColor = "white";
    // $(document).unbind('scroll'); 
    // $('body').css({'overflow':'visible'});
    document.querySelector(".post").style.display = "block";
}

var closeCss = function ()
{
    document.querySelector(".signUp-wrapper").style.display = 'none';
    document.body.style.backgroundColor = "white";
    // $(document).unbind('scroll'); 
    // $('body').css({'overflow':'visible'});
    document.querySelector(".post").style.display = "block";
}



function init()
{
    //own jquery plugin for visibility(css) 
    jQuery.fn.visible = function() {
        return this.css('visibility', 'visible');
    };
    
    jQuery.fn.invisible = function() {
        return this.css('visibility', 'hidden');
    };
    
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.querySelector('#dynamic-menu');
        // Check user login
        if (user) {
            // $(document).unbind('scroll'); 
            // $('body').css({'overflow':'visible'});
            user_email = user.email;
            menu.innerHTML = "<button class='btn btn-danger mr-sm-2 mb-1' type='button' id='newPostBtn'>Create a new post</button><div class='dropdown '><button class=' mb-1 btn btn-info dropdown-toggle dropdown-toggle-split' data-toggle='dropdown' id='email'>" + user.email + "</button>" + "<div class='dropdown-menu'> <a class='dropdown-item' id='profileBtn'>My profile</a> <a class='dropdown-item' id='logout-btn'>Log Out</a> </div> </div>";
            document.querySelector("#logout-btn").addEventListener("click", function(){firebase.auth().signOut();window.location = 'index.html';});
            //the function below is run after login, so put here
            createNewPost();
            closeButtonpP();
            profileBtn();
            
        } else {
            // // It won't show any post if not login
            //menu.innerHTML = '<button class="btn btn-light" type="button" id="signInBtn">Log In</button><button class="btn btn-primary mx-2" type="button" id="signUpBtn">Sign Up</button>'
            // document.getElementById('post_list').innerHTML = "";
        }
    });

}

function googleSignIn()
{
    //googleSignIn
    var btnGoogle = document.querySelector('#btnGoogle');

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"

        var provider = new firebase.auth.GoogleAuthProvider();

        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
        }).catch(function (error) {
            console.log('error: ' + error.message);
        });
        firebase.auth().onAuthStateChanged(user => {
            if(user) {
              window.location = 'index.html'; //After successful login, user will be redirected to
            }
          });
    });
}

function googleSignIn2()
{
    //googleSignIn
    var btnGoogle = document.querySelector('#btnGoogle2');

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"

        var provider = new firebase.auth.GoogleAuthProvider();

        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
        }).catch(function (error) {
            console.log('error: ' + error.message);
        });
        firebase.auth().onAuthStateChanged(user => {
            if(user) {
              window.location = 'index.html'; //After successful login, user will be redirected to
            }
          });
    });
}

function fbSignIn()
{
    var facebook_provider = new firebase.auth.FacebookAuthProvider();
    var btnLoginFBRedi = document.querySelector('#btnFacebook');

    btnLoginFBRedi.addEventListener('click', e => {
        firebase.auth().signInWithRedirect(facebook_provider);
    });
}

function fbSignIn2()
{
    var facebook_provider = new firebase.auth.FacebookAuthProvider();
    var btnLoginFBRedi = document.querySelector('#btnFacebook2');

    btnLoginFBRedi.addEventListener('click', e => {
        firebase.auth().signInWithRedirect(facebook_provider);
    });
}

function post()
{    
    post_btn = document.querySelector('#postBtn');
    post_title = document.querySelector('#postTitle');
    post_txt = document.querySelector('#comment');
    var postsRef = firebase.database().ref('postList');
       

    post_btn.addEventListener('click', function () {
        //can scroll again
        // $(document).unbind('scroll'); 
        // $('body').css({'overflow':'visible'});
        document.querySelector(".post").style.display = "block";
        if (post_txt.value != "") {
            var email = document.querySelector('#email');
            var postRef = firebase.database().ref(postsRef).child(categorySelection());
                postRef.push().set({
                    category: categorySelection(),
                    user_email: email.textContent,
                    title: post_title.value,
                    post: post_txt.value,
            }).then(function(){
                document.querySelector("#postPage").style.display = 'none';
                document.body.style.backgroundColor = "white";
                $("#recentPostTitle").html("Recent Post");
                console.log("新增Post成功");
            }).catch(function(err){
                console.error("新增Post錯誤：",err);
            })
            
            var postRef2 = firebase.database().ref("postList/recentPost");
                postRef2.push().set({
                    category: categorySelection(),
                    user_email: email.textContent,
                    title: post_title.value,
                    post: post_txt.value
            }).then(function(){
                console.log("新增Post成功");
            }).catch(function(err){
                console.error("新增Post錯誤：",err);
            })    
        }
        
        post_txt.value = "";
        post_title.value = "";
        
    });

    // The html code for post
    var str_before_username = "<div class='p-3 rounded box-shadow postJSRecentPost'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_middle = "</h6><div class='media text-muted pt-3'><i class='fas fa-pencil-alt mr-2 rounded' style='height:32px; width:32px;'></i><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark postContent'>";
    var str_after_content = "</p></div></div>\n";
    // var str_before_username = "<div class=''><h6 class''>";
    // var str_middle = "</h6><div class=''><p class=''><strong class='d-block text-gray-dark'>";
    // var str_after_content = "</p></div></div><br>";

    
    var postsRef3 = firebase.database().ref('postList/recentPost');
    var lastMessagesQuery = postsRef3.limitToLast(100);
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    lastMessagesQuery.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
           
            //the recent post will be at top by unshift
           //get all history post
           

            snapshot.forEach(childSnapshot => {
                var childData = childSnapshot.val();
                total_post.unshift(str_before_username + "<span class='post_title' title='Click me to comment' onclick='redirectToFullPage(this.textContent)'>" + childData.title + "</span>" + str_middle + childData.post + "</strong>Started by " + "<span id='post_name'>" + childData.user_email + "</span>" + str_after_content);
                first_count += 1;
                
            });
            document.querySelector("#post_list").innerHTML = total_post.join(''); //join use for array, join('') means no comma between data
            
            //update new post
            // child_added: Retrieve lists of items or listen for additions to a list of items. This event is triggered once for each existing child and then again every time a new child is added to the specified path. The listener is passed a snapshot containing the new child's data.
            lastMessagesQuery.on('child_added', function(data){
                second_count += 1;
                if(second_count > first_count)
                {
                    var childData = data.val();
                    //total_post[total_post.length] = str_before_username + childData.user_email + "</strong>" + childData.data_name + str_after_content;
                    total_post.unshift(str_before_username + "<span class='post_title' title='Click me to comment' onclick='redirectToFullPage(this.textContent)'>" + childData.title + "</span>" + str_middle + childData.post + "</strong>Started by " + "<span id='post_name'>" + childData.user_email + "</span>" + str_after_content);
                    document.querySelector("#post_list").innerHTML = total_post.join('');
                    if(Notification.permission!=='default'){
                        var notify;
                        
                        notify= new Notification('New message from '+ data.val().user_email,{
                            'body': "Post title: " + data.val().title,
                            'icon': 'image/icon/group.png'
                        });
                    }
                }
            });
        })
        .catch(e => console.log(e.message));
}

function categorySelection()
{
    var select = document.querySelector("#selectpicker");
    
    return select.value;
}

function createNewPost()
{
    var newPostBtn = document.querySelector("#newPostBtn");  
    newPostBtn.addEventListener("click", createNewPostCss);
}

var createNewPostCss = function()
{
    document.querySelector("#postPage").style.display = "block";
    document.body.style.backgroundColor = "rgba(0, 0, 0, 0.4)";
    document.querySelector(".signIn-wrapper").style.display = 'none';
    document.querySelector(".signUp-wrapper").style.display = 'none';
    document.querySelector(".post").style.display = "none";
    $(".comment").hide();
}


function redirectToFullPage(title)
{  
    //alert(title);
    $("#userPage").hide();
    $(".post").show();
    $(".comment").show();
    $("#newPostBtn").hide();
    $("#comment_list").show();
    $(".searchForm").visible();
    document.querySelector("#comment_list").innerHTML = ""; 
    document.querySelector("#post_list").innerHTML = ""; 
    var email = document.querySelector("#email");
    var postsRef = firebase.database().ref('postList/recentPost');
    var total_post = [];

    var str_before_username = "<div class='p-3 bg-white rounded box-shadow postJSRedirect'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_middle = "</h6><div class='media text-muted pt-3'><i class='fas fa-pencil-alt mr-2 rounded' style='height:32px; width:32px;'></i><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark postContent'>";
    var str_after_content = "</p></div></div>\n";
    var commentBtn = '<button type="button" id="commentBtn" onclick="commentPage()" class="btn btn-info btn-sm">Write a comment</button>';
    var likeViewver = '<div id="likeViewer"></div>';
    var voteBtn = '<button type="button" class="btn btn-primary btn-sm" id="inc" onclick="incId()">Like</button>';
    var commentBar = "<br><br><br><h6 class='border-bottom border-gray pb-2 mb-0' id='commentTitle'>Comments</h6>"
    var deleteBtn = '<button type="button" class="btn btn-danger btn-sm" id="deleteBtn" onclick="deletePost()">Delete</button>'


    postsRef.once('value').then(function (snapshot) {
        snapshot.forEach(childSnapshot => {
            var childData = childSnapshot.val();
            if(childData.title == title){
                category = childData.category;
                document.querySelector("#recentPostTitle").innerHTML = category;
                document.querySelector("#recentPostTitle").style.fontSize = "2rem"

                //if the below function put outside, the category will be undefined
                commentFunc(title, category);
                //detach listener, if not, the like will not show after voting it
                var fb = firebase.database().ref("incid/" + title);
                fb.child('counter').off('value');
                vote(title);
                // alert(title);
                // alert(category);
                total_post.push(str_before_username + "<span class='post_title_redirect_page font-weight-bold'>" + childData.title + "</span>" + str_middle + "<span class='post_redirect_page'>" +  childData.post + "</span>" + "</strong>Started by " + "<span id='post_name'>" + childData.user_email + "</span>" + str_after_content + likeViewver + voteBtn + commentBtn + deleteBtn + commentBar);
            }
        });
        document.querySelector("#post_list").innerHTML = total_post.join('');
        
    }).catch(e => console.log(e.message));
    
        
}

function commentPage()
{
    firebase.auth().onAuthStateChanged(function (user) {
        if(user){
            document.querySelector("#commentBox").style.display = "block";
            
        }else{
            alert("Please register or sign in to comment.");
        }
    });
}

function commentFunc(title, category)
{
    
    var postsRef = firebase.database().ref('commentList');
    var commentPostBtn = document.querySelector("#commentPostBtn");
    var commentPostCancelBtn = document.querySelector("#commentPostCancelBtn");
    post_txt = document.querySelector('.post-text-area');

    // alert(category);
    // alert(title);

    commentPostCancelBtn.addEventListener('click', function(){
        $("#commentBox").hide();
        post_txt.value = "";
    });

     // List for store posts html
     var total_post = [];
     // Counter for checking history post update complete
     var first_count = 0;
     // Counter for checking when to update new post
     var second_count = 0;
 
     var str_before_username = "<div class='p-3 bg-white rounded box-shadow postJS'><h6 class='border-bottom border-gray pb-2 mb-0'>";
     var str_middle = "</h6><div class='media text-muted pt-3'><img class='mr-2 rounded' src='image/login/avatar2.png' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark postContent'>";
     var str_after_content = "</p></div></div>\n";
 
     postsRef2 = firebase.database().ref(postsRef).child("" + category);
 
     
     postsRef2.once('value').then(function (snapshot) {
         snapshot.forEach(childSnapshot => {
             var childData = childSnapshot.val();
             if(childData.post_title == title){
                 total_post.push(str_before_username + childData.user_email + str_middle + childData.comment + str_after_content);
                 first_count += 1;
             } 
         });
         document.querySelector("#comment_list").innerHTML = total_post.join('');

           postsRef2.on('child_added', function(data){
                  second_count += 1;
                  if(second_count > first_count)
                  {
                     var childData = data.val();
                     //total_post[total_post.length] = str_before_username + childData.user_email + "</strong>" + childData.data_name + str_after_content;
                     if(childData.post_title == title){
                         total_post.push(str_before_username + childData.user_email + str_middle + childData.comment + str_after_content);
                         document.querySelector("#comment_list").innerHTML = total_post.join('');
                     }
                 }
             });

        
     }).catch(e => console.log(e.message));

    //cannot use addEventListener, if use arguments will Accumulate
    $("#commentPostBtn").click( function () {
        // alert(category);
        // alert(title);
        document.querySelector("#comment_list").innerHTML = "";
        if (post_txt.value != "") {
            var email = document.querySelector('#email');

            var postRef = firebase.database().ref(postsRef).child("" + category);
                postRef.push().set({
                    post_title: title,
                    user_email: email.textContent,
                    comment: post_txt.value,
            }).then(function(){
                document.body.style.backgroundColor = "white";
                document.querySelector("#commentBox").style.display = "none";
                console.log("新增comment成功");
            }).catch(function(err){
                console.error("新增comment錯誤：",err);
            }) 

            var postRef2 = firebase.database().ref(postsRef).child("recentComment");
                postRef2.push().set({
                    post_title: title,
                    user_email: email.textContent,
                    comment: post_txt.value,
            }).then(function(){
                console.log("新增comment成功");
            }).catch(function(err){
                console.error("新增comment錯誤：",err);
            }) 
        }
        post_txt.value = "";

    });
    
    

}

function redirectPostWhenClickCategory()
{
    
    $(document).on('click', '.label', function () {
        
        $( "#commentPostBtn" ).off();
        var category = this.textContent;
        //alert(category);
        document.querySelector("#post_list").innerHTML = "";
        document.querySelector("#comment_list").innerHTML = ""; 
        document.querySelector("#commentBox").style.display = "none";
        document.querySelector("#recentPostTitle").innerHTML = "" + category;
        $("#newPostBtn").show();
        $(".post").show();
        $("#userPage").hide();
        $(".searchForm").visible();
        
        var str_before_username = "<div class='p-3 rounded box-shadow postJSRecentPost'><h6 class='border-bottom border-gray pb-2 mb-0'>";
        var str_middle = "</h6><div class='media text-muted pt-3'><i class='fas fa-pencil-alt mr-2 rounded' style='height:32px; width:32px;'></i><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark postContent'>";
        var str_after_content = "</p></div></div>\n";

        
        
        var postsRef2 = firebase.database().ref('postList').child("" + category);
        // List for store posts html
        var total_post = [];
        // Counter for checking history post update complete
        var first_count = 0;
        // Counter for checking when to update new post
        var second_count = 0;

        postsRef2.once('value')
            .then(function (snapshot) {

                snapshot.forEach(childSnapshot => {
                    var childData = childSnapshot.val();
                    total_post.unshift(str_before_username + "<span class='post_title' title='Click me to comment' onclick='redirectToFullPage(this.textContent)'>" + childData.title + "</span>" + str_middle + childData.post + "</strong>Started by " + "<span id='post_name'>" + childData.user_email + "</span>" + str_after_content);
                    first_count += 1;
                    
                });
                document.querySelector("#post_list").innerHTML = total_post.join(''); //join use for array, join('') means no comma between data
                
                //update new post
                // child_added: Retrieve lists of items or listen for additions to a list of items. This event is triggered once for each existing child and then again every time a new child is added to the specified path. The listener is passed a snapshot containing the new child's data.
                postsRef2.on('child_added', function(data){
                    second_count += 1;
                    if(second_count > first_count)
                    {
                        var childData = data.val();
                        //total_post[total_post.length] = str_before_username + childData.user_email + "</strong>" + childData.data_name + str_after_content;
                        total_post.unshift(str_before_username + "<span class='post_title' onclick='redirectToFullPage(this.textContent)'>" + childData.title + "</span>" + str_middle + childData.post + "</strong>Started by " + "<span id='post_name'>" + childData.user_email + "</span>" + str_after_content);
                        document.querySelector("#post_list").innerHTML = total_post.join('');
                    }
                });
            })
            .catch(e => console.log(e.message));
         });
         
}

function profileBtn()
{
    var profileBtn = document.querySelector("#profileBtn");
    profileBtn.addEventListener("click", profileBtnCss);
}

var profileBtnCss = function()
{   
    $(".post").hide();
    $("#newPostBtn").hide();
    $(".comment").hide();
    $("#commentBox").hide();
    $("#userPage").show();
    $('.nav-link').removeClass('active');
    $("#navPost").addClass('active');
    userPagePost();
    $(".searchForm").invisible();

    $(document).on('click', '.nav-link', function () {
        $('.nav-link').removeClass('active');
        $(this).addClass('active');
        if(this.textContent == "Posts")
        {
            userPagePost();
        }
        else if(this.textContent == "Comments")
        {
            userPageComment();
        }
        else
        {

        }
    });   

}

function userPageComment()
{
    var navPost = document.querySelector("#navPost");
    var email = document.querySelector("#email");
    $("#userName").html(email.textContent);

    var str_before_username = "<div class='p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_middle = "</h6><div class='media text-muted pt-3'><i class='fas fa-pencil-alt mr-2 rounded' style='height:32px; width:32px;'></i><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('commentList/recentComment');
    // List for store posts html
    var total_post = [];

    postsRef.once('value')
        .then(function (snapshot) {          

            snapshot.forEach(childSnapshot => {
                var childData = childSnapshot.val();
                if(childData.user_email == email.textContent){
                    total_post.unshift(str_before_username + "<span class='post_title' title='Click me to comment' onclick='redirectToFullPage(this.textContent)'>" + childData.post_title + "</span>" + str_middle + childData.comment + str_after_content);
                } 
            });
            document.querySelector("#post_list_user").innerHTML = total_post.join(''); //join use for array, join('') means no comma between data
            
        })
        .catch(e => console.log(e.message));
}

function userPagePost()
{
    var navPost = document.querySelector("#navPost");
    var email = document.querySelector("#email");
    $("#userName").html(email.textContent);

    var str_before_username = "<div class='p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_middle = "</h6><div class='media text-muted pt-3'><i class='fas fa-pencil-alt mr-2 rounded' style='height:32px; width:32px;'></i><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark postContent'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('postList/recentPost');
    // List for store posts html
    var total_post = [];

    postsRef.once('value')
        .then(function (snapshot) {          

            snapshot.forEach(childSnapshot => {
                var childData = childSnapshot.val();
                if(childData.user_email == email.textContent){
                    total_post.unshift(str_before_username + "<span class='post_title' title='Click me to comment' onclick='redirectToFullPage(this.textContent)'>" + childData.title + "</span>" + str_middle + childData.post + str_after_content);
                } 
            });
            document.querySelector("#post_list_user").innerHTML = total_post.join(''); //join use for array, join('') means no comma between data
            
        })
        .catch(e => console.log(e.message));
}

function chromeNotification()
{
    //const messaging = firebase.messaging();
    // messaging.usePublicVapidKey('BMb-Ym8VarGwME8XEW-5kNkm_L6T34JO-3a0w1FAHWa3TlnjALHsiU-xoFb99YaEFNKzVvZ4FBegfBNaobB-sOM');
    Notification.requestPermission().then(function(){
        console.log("Have permission");
    })
    .catch(function(error){
        console.log("error for having permission");
    })

}

function vote(title)
{
    // var title = $(".post_title_redirect_page").text();
    var fb = firebase.database().ref("incid/" + title);
    //fb.child('counter').off('value');
    // monitors changes and updates UI

    fb.child('counter').on('value', updateDiv);
    // fb.on('value', updatePre);
    // for demo purposes
    function updateDiv(ss) {
        $('#likeViewer').text( (ss.val() || 0) + " likes" );
    }
}

// creates a new, incremental record
function incId() {
    var title = $(".post_title_redirect_page").text();
    var email = document.querySelector('#email');
    var fb = firebase.database().ref("incid/" + title);
    
    // increment the counter
    fb.child('counter').transaction(function(currentValue) {
        return (currentValue||0) + 1;
    });

}

function search()
{
    //var searchBtn = document.querySelector("#search");
    $("#search").off(); 
    
    $("#search").click (function(){
        $("#comment_list").hide();
        var searchTitle = $("#searchBar").val();
        var fb = firebase.database().ref("postList/recentPost");
        var childDataTitle = [];
        var postsRef = firebase.database().ref('postList/recentPost');
        var str_before_username = "<div class='p-3 rounded box-shadow postJSRecentPost'><h6 class='border-bottom border-gray pb-2 mb-0'>";
        var str_middle = "</h6><div class='media text-muted pt-3'><i class='fas fa-pencil-alt mr-2 rounded' style='height:32px; width:32px;'></i><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark postContent'>";
        var str_after_content = "</p></div></div>\n";
        var total_post = [];
        var i = 0;


        fb.orderByChild('title').startAt(searchTitle).endAt(searchTitle + "\uf8ff").on("child_added", function(snapshot) {
            $("#recentPostTitle").text("Search Post");
            childDataTitle.push(snapshot.val().title);
            postsRef.once('value')
            .then(function (snapshot) {          
                snapshot.forEach(childSnapshot => {
                    var childData = childSnapshot.val();
                    if(childData.title == childDataTitle[i]){
                        //alert(childDataTitle[i]);
                        total_post.push(str_before_username + "<span class='post_title' title='Click me to comment' onclick='redirectToFullPage(this.textContent)'>" + childData.title + "</span>" + str_middle + childData.post + str_after_content);
                        i++;
                        //alert(i);
                        document.querySelector("#post_list").innerHTML = total_post.join(''); 
                    } 
                });
            })
        });
         
    });

}

function deletePost()
{
    var postRef = firebase.database().ref("postList/recentPost");
    var commentRef = firebase.database().ref("commentList/recentComment");
    var category = $("#recentPostTitle").text();
    var postRefCategory = firebase.database().ref("postList/" + category);
    var commentRefCategory = firebase.database().ref("commentList/" + category);
    var title = $(".post_title_redirect_page").text();
    var email = $("#email").text();
    //alert(email);
    
    var postsRef3 = firebase.database().ref('postList/recentPost');
    var lastMessagesQuery = postsRef3.limitToLast(100);
    lastMessagesQuery.off('child_added');
    
    

    var deleteMessages = postRef.orderByChild('title').equalTo(title);
    deleteMessages.on('child_added', function(messagesSnapshot) {
        //messagesSnapshot.child().remove();
        //console.log(messagesSnapshot.key);
        if(messagesSnapshot.val().user_email == email)
        {
            alert("Please press F5 after press the delete button");
            postRef.child(messagesSnapshot.key).remove();
            var vote = firebase.database().ref("incid/");
            vote.child(title).remove();
        }
        else
        {
            alert("Only user can delete the post");
        }
        
    });

    var deleteMessages2 = commentRef.orderByChild('post_title').equalTo(title);
    deleteMessages2.on('child_added', function(messagesSnapshot) {
        
        if(messagesSnapshot.val().user_email == email)
        {
            commentRef.child(messagesSnapshot.key).remove();
        }
    });

    var deleteMessages3 = postRefCategory.orderByChild('title').equalTo(title);
    deleteMessages3.on('child_added', function(messagesSnapshot) {
        
        if(messagesSnapshot.val().user_email == email)
        {
            postRefCategory.child(messagesSnapshot.key).remove();
        }
    });

    var deleteMessages4 = commentRefCategory.orderByChild('post_title').equalTo(title);
    deleteMessages4.on('child_added', function(messagesSnapshot) {
        
        if(messagesSnapshot.val().user_email == email)
        {
            commentRefCategory.child(messagesSnapshot.key).remove();
        }
    });

    
    
    
    
}