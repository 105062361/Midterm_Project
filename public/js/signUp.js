window.onload = function () 
{
    init();
};

function init()
{
    var txtEmail = document.querySelector('#inputEmail');
    var txtPassword = document.querySelector('#inputPassword');
    var btnSignUp = document.querySelector("#btnSignUp");

    btnSignUp.addEventListener('click', function () {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        //alert("signUp");
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(e => console.log(e.message));
        firebase.auth().onAuthStateChanged(user => {
            if(user) {
              window.location = 'index.html'; //After successful login, user will be redirected to index.html
            }
          });
    });
}